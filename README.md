This is a small script to facilitate mass crafting in (vanilla) Minecraft.

No support exceeding this readme can be expected!

Notes:
- this is provided without any warranty, use at own risk!
- this script scans your screen content for certain contents
- it automatically performs input actions similar to a user based on its findings
- this might lead to highly unwanted results! There is no guarantee that its actions make any sense.
- crafting recipes including items with animated icons cannot be used so far
- so far only "simple" crafting recipes are usable, i.e., recipes which yield less or equal number of items than they require (e.g. ingots to block but not blocks to ingots; there is currently no check for sufficient inventory space and the scripts logic will fail if the is insufficient space!). Additionally all ingredients need to have the same stack size (such that the crafting grid is completely empty after shift+clicking on the output slot)

Requirements:
- xdotool (linux command line tool, use for some safeguards+convenience funtions)
- various python modules, see crafter.py for imports
- for proper functionality, autopy(https://github.com/msanders/autopy/) needs to be installed in a version with this (https://github.com/msanders/autopy/pull/35) pull request applied.

Usage:
- example: ./crafter.py iron_block [--drop]
- create (small) images of the items you want to use in your crafting recipes in the samples folder (file type and extension: 'png'). The script will search your screen for exact matches of these images and use their locations to create a list of mouse movements to perform the crafting
- create a similar image of the top half of the crafting screen (crafting grid + output slot), an image showing an empty crafting slot and one showing the empty output slot. The file names need to be 'crafting_grid.png', 'empty_slot.png' and 'output_slot.png'.
- if images for the crafting products are present, --drop option can be used to automatically throw out crafted items
- the file names (without extension) need to correspond to the naes used in crafting recipes
- crafting recipes are defined in 'recipes.txt' (see provided version for examples)
- to use multiple alternatives for an ingredient (different types of planks), alternatives can be defined in 'alternatives.txt'

