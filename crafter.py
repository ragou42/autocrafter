#!/usr/bin/env python2

from PIL import Image
from pyautogui import  *
import pyautogui
import sys
import argparse
from subprocess import *
from time import *
import autopy
from os import listdir
from os.path import isfile, join
import string

#some global definitions (in-code config)
recipeFileName = "recipes.txt"
alternativeFileName = "alternatives.txt"


pyautogui.PAUSE = 0.075

def loadSamples():
  files = [f for f in listdir("samples") if isfile(join("samples", f))]
  samples = {}
  for f in files:
    if f[-4:] == '.png': 
      bmp = autopy.bitmap.Bitmap.open(join("samples",f))
      f = f[:-4]
      samples[f] = bmp
  return samples
#actually load it into global scope  
samples = loadSamples()

def getAlternativeDict():
  lines = [line.rstrip('\n') for line in open(alternativeFileName)]
  alternatives = {}
  for line in lines:
    tmpList = string.split(line,"=",1)
    if len(tmpList)<2: continue
    name = tmpList[0].strip()
    ingredString = tmpList[1]
    ingredients = [i.strip() for i in string.split(ingredString,",")]
    if name in alternatives:
      print "[WARN] There is already a set of alternatives for '{:s}', ignoring new definition in line '{:s}'".format(name,line)
    
    #set unknown ingredients to empty slots:
    for i,ingr in enumerate(ingredients):
      if ingr not in samples:
        ingredients.pop(i)
    
    alternatives[name] = ingredients
  return alternatives
  
#actually load it into global scope
alternatives = getAlternativeDict()
#find MC window (used for flow control)  
window = check_output("xdotool search --onlyvisible --class 'Minecraft 1.10'", shell=True).rstrip('\n')



def loadRecipes():
  lines = [line.rstrip('\n') for line in open(recipeFileName)]
  recipes = {}
  for line in lines:
    tmpList = string.split(line,"=",1)
    if len(tmpList)<2: continue
    name = tmpList[0].strip()
    ingredString = tmpList[1]
    ingredients = [i.strip() for i in string.split(ingredString,",")]
    if len(ingredients) != 9: 
      print "[WARN] Wrong number of ingredients, must always be 9 elements. Ignoring line '{:s}'".format(line)
      continue
    if name in recipes:
      print "[WARN] There is already a recipe for '{:s}', ignoring new definition in line '{:s}'".format(name,line)
    
    #set unknown ingredients to empty slots:
    for i,ingr in enumerate(ingredients):
      if ingr not in samples and ingr not in alternatives:
        ingredients[i] = None
    
    recipes[name] = ingredients
  return recipes
  
def findAll(haystack,name):
  if type(name) is str:
    if not name in samples: return None
    needle = samples[name]
    if needle is None:
      return None
    found = haystack.find_every_bitmap(needle)
    if len(found)<1:
      return None
    result = []
    for tup in found:
      result.append( (tup[0],tup[1],needle.width,needle.height) )
    return result
  elif type(name) is list:
    result = []
    for n in name:
      if not n in samples: continue
      needle = samples[n]
      if needle is None:
        continue
      found = haystack.find_every_bitmap(needle)
      if len(found)<1:
        continue
      for tup in found:
        result.append( (tup[0],tup[1],needle.width,needle.height) )
    if len(result)>0:
      return result
  return None   
  
def findOne(haystack,name):
  if type(name) is str:
    if not name in samples: return None
    needle = samples[name]
    if needle is None:
      return None
    found = haystack.find_bitmap(needle)
    if found is None:
      return None
    return (found[0],found[1],needle.width,needle.height)
  elif type(name) is list:
    for n in name:
      if not n in samples: continue
      needle = samples[n]
      if needle is None:
        continue
      found = haystack.find_bitmap(needle)
      if found is None:
        continue
      return (found[0],found[1],needle.width,needle.height)
  return None #if nothing succeeded
  
  
  
  
def makeScreenshot():
  return autopy.bitmap.capture_screen()
  
  
def isActive():
  return check_output("xdotool getactivewindow", shell=True).rstrip('\n') == window
  
def waitForActive():
  while (not isActive()):
    pass
  sleep(0.1)
  return

def clickAt(pos):
  x,y = center(pos)
  click(x, y)

def getGrid():
  #get the region of the crafting grid
  haystack = autopy.bitmap.capture_screen()
  region = findOne(haystack,"crafting_grid")  
  if not region:
    return None
  #find the actual crafting grid places
  slots = []
  #for pos in locateAllOnScreen(samples["em.pty_slot"],grayscale=True,region=region):
  for pos in findAll(haystack,"empty_slot"):
    slots.append(pos)
  slots = slots[:9]
  #if len(slots) != 9:
  #  print "Found incorrect number of crafting slots!"
  #  print slots
  #  return None
  
  #slots.append(locateOnScreen(samples["output_slot"],grayscale=True,region=region))
  slots.append(findOne(haystack,"output_slot"))
  return slots

def makePlan(recipe):
  if len(recipe)!= 9 : 
    print "[ERROR/makePlan] recipe must contain 9 entries in any case!"
    return None
  recipe = [str(x).strip() for x in recipe] #sanitize recipe components
  ingredients = {x:recipe.count(x) for x in recipe}
  positions = {}
  screenshot = makeScreenshot()
  for key in ingredients:
    if key=="" or key=="None": continue
    pos = findAll(screenshot,alternatives[key] if key in alternatives else key)
    if pos is None: return None
    positions[key] = pos
  
  nIterations = min([int(len(positions[x])/recipe.count(x)) for x in positions] )
  #print "Iterations: "+str(nIterations)
  plan = []
  for it in range(0,nIterations):
    for ingred in recipe:
      if ingred=="None" or ingred=="":
        plan.append(None)
        continue
      plan.append(positions[ingred].pop())
      
  return plan
  #print ingredients

def dropItem(name):
  haystack = makeScreenshot()
  searchFor = [name]
  if name in alternatives:
    searchFor += alternatives[name]
  positions = findAll(haystack,searchFor)
  if positions is None: return
  keyDown('ctrl')
  for p in positions:
    moveTo(center(p))
    press('q')
  keyUp('ctrl')
  return True
  

def craft(grid,ingredients):
  
  if ingredients is None or grid is None:
    return False
  didCraft = False
  while(len(ingredients) >= 9):
    for (index,gridPos) in enumerate(grid):
      if (index<9): #fill the crafting grid
        ingredient = ingredients.pop() 
        if ingredient is None: continue
        clickAt(ingredient)
        clickAt(gridPos)
      else: #retrieve the crafting output
        keyDown('shift')
        clickAt(gridPos)
        keyUp('shift')
        didCraft = True
        moveTo((gridPos[0]+50,gridPos[1]))
  return didCraft

def main(args):
  waitForActive()
  recipeName = args.recipe
  recipe = None
  listOfRecipeNames = string.split(recipeName,",")
  
  
  allRecipes = loadRecipes()  
  
  
  #if recipeName not in allRecipes:
  #  print "No recipe '{:s}' found!".format(recipeName)
  #  sys.exit()
    
  recipeList = [ [r,allRecipes[r.strip()]] for r in listOfRecipeNames if r.strip() in allRecipes]
  if len(recipeList)<1:
    print "No recipes found"
    sys.exit()
  
  
  while isActive():
    grid = None
    while grid is None:
      grid = getGrid()
      if not isActive():
        print "MC lost focus, exiting"
        sys.exit()

    for r in recipeList:
      if (craft(grid,makePlan(r[1])) and args.drop):
        dropItem(r[0])
    #craft(grid,recipe())
  


if __name__ == "__main__":
  # parse the CLI arguments
  parser = argparse.ArgumentParser(description='Autocrafting help for Minecraft')
  parser.add_argument('recipe', metavar='RECIPE', type=str,
            default="iron_block",
            help='item to be crafted')
  parser.add_argument('--drop', action="store_const", const=True, default=False, help='Drop crafting output')
  args = parser.parse_args()
  sys.argv = []
  
  if (args.drop):
    print "Going to drop crafting outputs"
  
  
  main(args)  
  print "Didn't crash! :)"  

